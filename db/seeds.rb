p1 = Project.create(name: 'Project one', platform: 'Platform 1', funding_amount: 50_000, city: 'Berlin', developer: 'dev a', duration_in_months: 12, interest_rate: 5.25)

p1.project_events.build(time: 10.days.ago.to_i, amount: rand(10_000))
p1.project_events.build(time: 9.days.ago.to_i, amount: rand(1_000))
p1.project_events.build(time: 8.days.ago.to_i, amount: rand(39_000)) # fully founded in three days

p1.save

p2 = Project.create(name: 'Project two', platform: 'Platform 1', funding_amount: 500_000, city: 'Paris', developer: 'dev c', duration_in_months: 24, interest_rate: 7.25)

p2.project_events.build(time: 10.days.ago.to_i, amount: rand(10_000))
p2.project_events.build(time: 9.days.ago.to_i, amount: rand(10_000))
p2.project_events.build(time: 8.days.ago.to_i, amount: rand(100_000)) # not fully founded

p2.save

p3 = Project.create(name: 'Project one', platform: 'Platform 2', funding_amount: 50_000, city: 'Berlin', developer: 'dev b', duration_in_months: 12, interest_rate: 5.25)

p3.project_events.build(time: 10.days.ago.to_i, amount: rand(10_000))
p3.project_events.build(time: 9.days.ago.to_i, amount: rand(10_000))
p3.project_events.build(time: 8.days.ago.to_i, amount: rand(10_000))
p3.project_events.build(time: 7.days.ago.to_i, amount: rand(10_000))
p3.project_events.build(time: 6.days.ago.to_i, amount: rand(10_000)) # fully founded in 5 days

p3.save
