class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :platform
      t.integer :funding_amount
      t.string :city
      t.string :developer
      t.integer :duration_in_months
      t.float :interest_rate

      t.timestamps
    end
  end
end
