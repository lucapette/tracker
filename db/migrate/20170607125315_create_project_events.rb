class CreateProjectEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :project_events do |t|
      t.timestamp :time
      t.integer :amount

      t.belongs_to :project
    end
  end
end
