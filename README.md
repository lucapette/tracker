# projects tracker

# How to test

Please run:

```sh
$ rake db:seed
```

to populate the dev db. I created a few projects with events so that it's
easier to look at UI.

To run the tests:

```sh
$ rake spec
```

As you can see, I covered only requests specs. I believe only t ath end to end
testing is *required* while controllers/unit tests are more about the workflow
and the taste of the developer writing the tests.  (Maybe a bit of an
unpopular opinion but I think a face to face discussion would clarify why end
to end testing is so important to me and why "the rest" isn't).

# Questions from the challenge

I think the questions can ben answered with a set of two features clustered by
the entity for which we want more anwers. Here is the current split:

## Project page

- How long did a project need to get fully funded?
- By how much did a project progress between two points in time?
- How long was a project online and open for investments?

## Company page

- What is the overall funding amount for a company based on the projects?
- Which projects progress faster than others?
- Which competitor is seeing the fastest progress with their projects?

# Notes

In the following, there are the notes I took while working on the project. I
decided it makes sense to share them so you can see how I think about the
project.

## Things I don't like

- I would prefer to have platform as a table in the database. I didn't get
  there yet. I may have the time to change this at the end.
- I've been asked to produce "production ready" code but there's not a
  mono-dimensional definition of "production ready". What I'm writing reflects
  that definition in my view, given the 8 hours constraints. In a real world
  project that is not a prototype, I would architect the applications in two
  separate parts: a rails API project (using Sequel instead of ActiveRecord)
  and a React/Recharts frontend. It scales better and leads to better
  reusability of the different compontents. I didn't do it that way because
  setting up the app would have taken a big part of the 8 hours and then you
  couldn't see any "real world" code from me.

## Things I like

- I like the challenge itself. I have to say that it's a lot of questions
  given the time constraint and the fact there's no pre-existing application.
  So the bootstrap time + the many questions puts some pressure on the person
  doing the challenge. If that's by design, I'm not sure it's a good thing.
  What I like about it the most, it's the fact that it makes you think already
  about the kind of problem the company has and it's a nice "real world"
  problem to solve. That's rare! Most code challenges are basically math
  quizzes and that really doesn't help.
