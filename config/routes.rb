Rails.application.routes.draw do
  post 'track', to: 'tracker#track', defaults: {format: :json}

  root to: 'welcome#index'
end
