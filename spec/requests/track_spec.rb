require 'rails_helper'

RSpec.describe "Track", type: :request do
  describe "POST /track" do
    # For the 4xxx cases I'm testing the error message because I think that
    # makes sense only if we're dealing with a public API. I'm not sure that's
    # the case, so I'm "saving" some little time.

    describe "when payload has missing required params" do
      # This is somewhat uncommon in RSpec. I've been doing this for a long
      # time and had the chance to discuss it with a RSpec maintainer (I'm a
      # proud contribution :)) and they say it's pretty nice. Not sure why
      # it's not more widespread.
      [:project, :platform, :capture_details].each do |param|
        it "returns unprocessable entity for #{param}" do
          post track_path, params: {param => "value"}

          expect(response).to have_http_status(422)
        end
      end
    end

    describe "when payload is valid" do
      let(:project) do
        {
          project: "project 1",
          platform: "company a",
          capture_details: {
            time: Time.now.to_i,
            amount: 4242,
          }
        }
      end

      it "creates a new project" do
        post track_path, params: project

        expect(response).to have_http_status(200)
        expect(Project.count).to eql(1)
      end

      it "does NOT duplicate projects" do
        Project.create({name: "project 1", platform: "company a"})

        post track_path, params: project

        expect(response).to have_http_status(200)
        expect(Project.count).to eql(1)
      end

      it "creates a new project with details" do
        with_details = project.merge({
          project_details: {
            funding_amount: 42,
            city: "Berlin",
            developer: "dev a",
            duration_in_months: 12,
            interest_rate: "5.75",
          }
        })

        post track_path, params: with_details

        expect(response).to have_http_status(200)

        # I'm assuming we return the whole object back for practical reasons I
        # wouldn't necessarly return the entire object back. Even though, if
        # it's true that there's at max an event per day, it takes years
        # before the payload grows too big.
        expect(response.body).to eql(Project.first.to_json)
      end

      # This test may feel a bit overkill but I don't like the fact I have to
      # handle the conversion on my on the controller and I have seen way too
      # many bugs in event tracking (timezones, formats.. and so on) not to
      # cover the specific field with its own test.
      it "stores the right timestamp" do
        a_week_ago = 1.week.ago.to_i
        specific_event = project.merge(capture_details: {
          time: a_week_ago,
          amount: 4242,
        })

        post track_path, params: specific_event

        expect(response).to have_http_status(200)

        expect(Project.first.project_events.first.time.to_i).to eql(a_week_ago)
      end
    end
  end
end
