class TrackerController < ApplicationController
  rescue_from ActionController::ParameterMissing do |e|
    render json: {error: e}, status: 422
  end

  def track
    # In the model, having the name of the project as "project" instead of
    # "name" would feel strange
    project_keys = project_params.slice(:project, :platform)
    project_keys[:name] = project_keys.delete(:project)

    # I'm assuming two projects can have the same name for different platform.
    # It is a strong assumption. The basic assumption is that platform is the
    # company name therefore it can happen that name isn't the natural key.
    project = Project.find_or_create_by(project_keys)

    # In the specs there's no specifications about the behaviors of the
    # details, so I'm updating them every time the endpoint gets hit. In a
    # real world endpoint, this may be a strange (and undesired side-effect).
    if project_params[:project_details]
      project.update_attributes!(project_params[:project_details])
    end

    event = project_params[:capture_details]

    event[:time] = Time.at(event[:time].to_i)
    project.project_events.build(event)

    project.save!

    render json: project, status: 200
  end

  private
  def project_params
    params.require(:project)
    params.require(:platform)
    params.require(:capture_details)

    params.permit(:name,
                  :platform,
                  :project,
                  project_details: [
                    :funding_amount,
                    :city,
                    :developer,
                    :duration_in_months,
                    :interest_rate
                  ],
                  capture_details: [
                    :time,
                    :amount,
                  ]
                 )
  end
end
