class Project < ApplicationRecord
  has_many :project_events

  def as_json(opts={})
    {
      project: name,
      platform: platform,
      project_details: {
        funding_amount: funding_amount,
        city: city,
        developer: developer,
        duration_in_months: duration_in_months,
        interest_rate: interest_rate,
      }
    }
  end
end
